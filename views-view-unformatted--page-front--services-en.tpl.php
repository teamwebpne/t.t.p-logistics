<section id="jump" class="layout_2_padding light-silver">
    <div class="row wow fadeIn" data-wow-duration="1.5s">
        <div class="col-md-12 col-xs-12">
            <div class="main-heading center_text">
                <h2>Our services</h2>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="service">
                <?php foreach ($rows as $id => $row): ?>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="row">
                        <?php print $row; ?>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
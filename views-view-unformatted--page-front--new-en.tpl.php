<section class="layout_2_padding">
    <div class="container wow fadeIn" data-wow-duration="1.5s">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="main-heading center_text">
                    <h2>News</h2>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="row">
                    <?php foreach ($rows as $id => $row): ?>
                        <div class="col-md-4 col-xs-12">
                            <?php print $row; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
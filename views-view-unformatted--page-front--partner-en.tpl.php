<div class="brands light-silver">
    <div class="container wow fadeIn" data-wow-duration="1.5s">
        <div id="partner" class="owl-carousel owl-theme">
            <?php foreach ($rows as $id => $row): ?>
                <?php print $row; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>

(function ($) {
    Drupal.behaviors.mymodule = {
        attach: function (context, settings) {
            "use strict";

            // $('#partner').owlCarousel({
            //     loop:true,
            //     margin: 15,
            //     autoplay: true,
            //     responsiveClass:true,
            //     autoplayHoverPause:true,
            //     responsive:{
            //         0:{
            //             items:1,
            //             stagePadding: 100,
            //         },
            //         600:{
            //             items:2,
            //         },
            //         1000:{
            //             items:6,
            //         }
            //     }
            // });
            $('#main-banner').owlCarousel({
                loop:true,
                margin:10,
                autoplay: true,
                responsiveClass:true,
                dots: false,
                nav: true,
                navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
                autoplayHoverPause:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false
                    },
                    600:{
                        items:1,
                        nav:false
                    },
                    1000:{
                        items:1,
                        nav:true,
                    }
                }
            });


            $("#navbar_menu").menumaker({
                title: "Menu",
                format: "multitoggle"
            });

            /**== wow animation ==**/

            new WOW().init();

            $(".bg_load").fadeOut("slow");

            $('.fader').hover(function() {
                $(this).find("img:last").fadeToggle();
            });

            $('.field-phone').attr('pattern','[+/0-9]{1,5}[0-9]{9,20}');
            $('.field-phone').attr('oninvalid','this.setCustomValidity(\'Phone numbers must be in 0987xxx or +84987xxx format\')');
            $('.fs-services').attr('href','#jump');

            $("a[href^='#']").click(function(e) {
                e.preventDefault();

                var position = $($(this).attr("href")).offset().top;

                $("body, html").animate({
                    scrollTop: position
                }, 1000 );
            });

        }
    }
}(jQuery));


	
